import axios from 'axios';
import { getToken } from '@/utils/token';

const api = axios.create({
    // baseURL: 'http://easycookapi.dummybox.my.id'
    baseURL: 'http://127.0.0.1:8000'
});


api.interceptors.request.use((config) => {
    const token = getToken();
    if (token) {
        config.headers.Authorization = `Token ${token}`;
    }
    return config;
});

export default api;
