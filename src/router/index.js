import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('../views/Home.vue')
        },
        {
            path: '/recipe/:id',
            name: 'recipe-detail',
            component: () => import('../views/RecipeDetail.vue')
        },
        {
            path: '/my-pantry',
            name: 'my_pantry',
            component: () => import('../views/MyPantry.vue')
        },
        {
            path: '/search',
            name: 'search',
            component: () => import('../views/SearchResult.vue')
        },
        {
            path: '/login',
            name: 'auth.login',
            component: () => import('../views/auth/Login.vue')
        },
        {
            path: '/register',
            name: 'auth.register',
            component: () => import('../views/auth/Register.vue')
        },
        {
            path: '/profile',
            name: 'profile.account',
            component: () => import('../views/profile/Account.vue')
        },
        {
            path: '/profile/favorite',
            name: 'profile.favorite',
            component: () => import('../views/profile/Favorite.vue')
        },
        {
            path: '/profile/history',
            name: 'profile.history',
            component: () => import('../views/profile/History.vue')
        },
    ]
})

export default router
